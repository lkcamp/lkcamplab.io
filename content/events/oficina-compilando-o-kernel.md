---
title: "Oficina 1: Compilando o kernel Linux"
date: 2024-08-14
draft: false
categories: ["Eventos"]
---

## Antes da oficina

Como nessa oficina será necessária a instalação de alguns pacotes e o download da árvore do kernel, você pode se adiantar com algumas etapas do processo.

### 1. Você pode instalar os pré-requisitos que usaremos:

a. Para **Ubuntu/Debian e derivados:**

```bash
sudo apt -y install git git-email fakeroot build-essential ncurses-dev xz-utils libssl-dev bc flex libelf-dev bison qemu-system debootstrap
```

b. Para **Fedora e derivados:**

```bash
sudo dnf install -y git-all make flex bison binutils qemu debootstrap ncurses ncurses-devel
```

c. Para **Arch Linux e derivados**:

```bash
sudo pacman -S --needed base-devel git util-linux bash binutils bc qemu-full debootstrap ncurses
```

### 2. Você também pode clonar a árvore do kernel (rode esse comando na pasta que você pretende realizar as oficinas):

```bash
git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
```

## Oficina

Seguir essa página em paralelo com o [tutorial](https://docs.lkcamp.dev/intro_tutorials/boot).

### 0. Organização das oficinas

Essas oficinas foram desenvolvidas para abordar de maneira mais prática o desenvolvimento do kernel Linux. Tangenciando de maneira superficial as questões mais teóricas.

Nosso objetivo é ajudar no início da jornada prática de um desenvolvedor para kernel Linux. Apontaremos algumas referências teóricas para o contínuo aprendizado.

Nem Linus Torvalds entende tudo que acontece no Linux hoje em dia, então não se cobre de aprender tudo de uma vez. Nós também somos aprendizes nessa jornada e estamos muito felizes de ter mais companhia para ela. :)

Cada oficina terá 2 links de referência: uma parte mais descritiva, escrita em formato de [blog (em português)](https://lkcamp.dev/events/) que servirá como guia durante o dia da oficina e uma parte mais técnica escrita em forma de [tutorial (em inglês)](https://docs.lkcamp.dev/) seguindo o formato de documentação do Linux e que tem o objetivo mais prolongado de referência, com maior preocupação de atualização.

### 1. O grande projeto kernel Linux

O kernel Linux é o elemento principal de um sistema operacional. O kernel é o programa que interpreta necessidades das aplicações em ações para o hardware e interpreta limitações do hardware para distribuir recurso para as aplicações.

![](/imgs/events/oficina-compilando-o-kernel/kernel-user-space.png)

O kernel Linux é apenas um grande projeto em linguagem C. Então nessa oficina 1 estaremos preocupados em compilar esse projeto e rodá-lo com sucesso para vê-lo.
Compreendida a etapa de compilação e execução do kernel, podemos brincar um pouco em algumas partes do código e algumas configurações.

### 2. Criação de um ambiente de teste (virtualização de uma máquina)

Como o kernel tem contato direto com o hardware do computador, pode ser uma boa ideia criar um espaço separado no seu disco e com uma ferramenta de emulação testar as alterações do kernel.
Pois a criação de um espaço virtual permite que mesmo que alguma alteração 'quebre' o kernel, o sistem da sua máquina se manterá intacto.

Para isso, vamos criar um espaço separado do seu disco e formatá-lo em `ext4` para criar um sistema de arquivos nesse espaço.
Com esse espaço separado e formatado, podemos instalar Linux e teremos um espaço virtual para testar.
Na [etapa 1 do tutorial booting](https://docs.lkcamp.dev/intro_tutorials/boot/#1-preparing-a-virtual-machine) você é capaz de criar seu espaço virtual.

### 3. Compilando e rodando

O kernel Linux, assim como qualquer outro projeto (principalmente em linguagem C) grande, possui um `Makefile`. Esse arquivo define as diretrizes de como o projeto será compilado dependendo da referência mandada em `make <referencia>`. Você pode vasculhar as opções que esse arquivo te dá, a princípio iremos ficar apenas nas opções de configuração `*config`. 

Você verá mais a frente (na oficina 3) que o kernel é divido em módulos (`module`). E cada opção de configuração seleciona quais módulos serão compilados com o kernel, isso definirá o tempo que levará para compilar seu kernel e quais funcionalidades você terá disponível.

Na [etapa 2 do tutorial booting](https://docs.lkcamp.dev/intro_tutorials/boot/#2-building-and-booting-the-kernel) você é capaz de compilar seu kernel e rodá-lo em seu ambiente de teste com uma ferramenta de emulação de sistema: `qemu`.

### 4. Brincando com kernel

Nesse momento você já deve ter entendido que o kernel é um projeto que pode ser compilado e testado.

Então a partir desse momento, sinta-se livre para escrever no kernel, com a função `pr_info("str")`; ou até mesmo 'quebrá-lo', chamando a função `panic("str")`.

Você pode também mudar as configurações dos módulos como quiser e dar o nome que desejar para seu kernel com `make menuconfig`.

Visite a [parte extra do tutorial booting](https://docs.lkcamp.dev/intro_tutorials/boot/#3-extra-playing-with-kernel) para brincar um pouco com seu kernel. :)
