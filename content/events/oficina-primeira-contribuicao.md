---
title: "Oficina 2: Processo de contribuição"
date: 2024-08-21
draft: false
categories: ["Eventos"]
---

### 0. Da oficina anterior:

Caso você tenha clonado a árvore do kernel com a flag `--depth=1` e agora queira recuperar a árvore toda, você pode dar `unshallow`:

```bash
git pull --unshallow
```

## Antes da oficina

### 1. Pegando o galho (`branch`) `staging`

Nessa oficina vamos aprender mais sobre o processo de contribuição.
Procurando na branch `staging` por alterações tangíveis de serem feitas durante a oficina.

Para adiantar, vamos adicionar esse `remote`, na raiz da sua pasta `linux`:

```
git remote add staging git://git.kernel.org/pub/scm/linux/kernel/git/gregkh/staging.git
git fetch staging
```

### 2. Se você estiver usando Arch Linux ou derivados tem alguns outros pré-requisitos para essa oficina:

Unicamente **Arch Linux e derivados**:

```bash
sudo pacman -S --needed perl-authen-sasl perl-io-socket-ssl
```

### 3. Configurar e-mail

Nessa oficina usaremos o `git send-email` para enviar os commits.

Alguns servidores de e-mail bloqueiam essa ferramenta.

#### a. Para quem usa **gmail**:

Ative a ferramenta que permite que apps menos seguros usem o email: [myaccount.google.com/lesssecureapps](https://myaccount.google.com/lesssecureapps)

O que podemos fazer para aumentar a segurança,
quando permitirmos que apps menos seguros usem a conta é ativar a função de autenticação em 2 fatores do gmail:
[myaccount.google.com/signinoptions/two-step-verification](https://myaccount.google.com/signinoptions/two-step-verification)

Com a autenticação em 2 fatores ligada, você poderá gerar uma senha específica para acesso do app ao seu email: [security.google.com/settings/security/apppasswords](https://security.google.com/settings/security/apppasswords).
(Obs: copie essa senha em algum lugar, pois ela não aparecerá novamente)

#### b. Outros provedores de email:

Para outros provedores, podemos tentar ajudar no dia, mas no geral vocês podem se adiantar pesquisando como fazer "git send-email <provedor>".

(Obs: Não encontramos formas fáceis de realizar isso no outlook.)

### 4. Contato com a comunidade

A partir desse momento, entraremos mais em contato com a(s) comunidade(s) do kernel.
Então se você quiser ler com calma um documento que preparamos sobre como entrar em contato com a comunidade,
visite: [Reaching out to the community](https://docs.lkcamp.dev/unicamp_group/reaching_community/).

Para se comunicar nas comunidades do kernel, lembre-se sempre de seguir o [código de conduta](https://docs.kernel.org/process/code-of-conduct.html).

Ao considerar postar algo em listas de e-mail, lembre-se de ser respeitoso e nunca mande mensagens "teste" em listas de e-mail.

## Oficina

Seguir essa página em paralelo com o [tutorial](https://docs.lkcamp.dev/intro_tutorials/first_contribution/).

### Ciclo de desenvolvimento

Apesar de muito grande, o projeto Linux Kernel é extremamente ramificado.
Por exemplo, na versão 6.6 durante 9 semanas 14069 commits foram realizados: aproximadamente 6 commits por minuto (<https://lwn.net/Articles/948970/>).
Essas contribuições foram realizadas por 1978 desenvolvedores,
em que 249 deles contribuíam pela primeira vez no kernel nesse ciclo.

Com essa quantidade de commit e desenvolvedores, não é possível manter esse projeto em uma árvore única com merge requests constantes no gitlab ou github.

Então como isso é feito:
As alterações são absorvidas por árvores secundárias que são gerenciadas por mantenedores.
E a cada nova versão do Kernel (aproximadamente a cada 9 semanas), temos um período de 2 semanas de 'merge window'
em que as alterações coletadas de cada árvore são agregadas na árvore oficial e serão disponibilizadas na nova versão do kernel.

![](/imgs/events/oficina-primeira-contribuicao/development_cycle.png)

### Árvores de subsistemas

Cada subsistema é mantido por um ou mais mantenedores, para os subsistemas que tem árvore própria, e.s mantenedore.s responsáveis deverão:
- manter a árvore do subsistema atualizada com a árvore Linux oficial
- absorver novos commits relativos a seu subsistema, mantendo a consistência da árvore
- enviar todos os novos commits da árvore para a árvore oficial na próxima 'merge window'

Você pode ver as árvores de cada subsistema simplesmente vendo o arquivo `MAINTAINERS` na raiz do git - a linha `T:` indica a árvore e a.s linha.s `M:` e.s mantenedore.s responsáveis.


Normalmente novos commits serão aceitas e adicionadas em uma branch `-next` da árvore do subsistema que está sendo contribuído. 
Como nessa oficina contribuiremos para a árvore `staging`, estaremos de olho na branch `staging-next`.

### 1. Como fazer uma contribuição, email, listas

Além disso, as contribuições dentro do projeto tem um volume tão grande que a ferramenta adotada para troca de informações e patches é email.

Listas de email conseguem carregar um grande volume de informações em formato 'cru' de texto.
Basta usar a opção 'plain text' de email (listas não aceitam emails HTML).

O git, que originalmente foi criada para gerenciar o versionamento do projeto Linux, tem uma ferramenta para envio de commits por email `git send-mail`.
Então, podemos já configurar nossa máquina para isso. Siga as instruções da [etapa 1 do tutorial](https://docs.lkcamp.dev/intro_tutorials/first_contribution/#1-preparing-your-tools).

Além disso, vamos ficar de olho no que o grupo faz, se inscreva nas listas de email em que vamos
enviar os patches: (linux-staging+subscribe@lists.linux.dev) e (~lkcamp/patches+subscribe@lists.sr.ht).
Basta enviar um email vazio para esse endereço e depois confirmar a inscrição na lista.

### 2. Estilo de código, regras de conduta

O projeto Linux tem suas regras de código, aplicadas à maneira de desenvolvimento: <https://www.kernel.org/doc/html/v4.10/process/coding-style.html>.

O projeto Linux tem suas regras de commit, aplicadas à maneira de escrita das mensagens de commit: <https://docs.kernel.org/process/submitting-patches.html>.

E o projeto Linux tem suas regras de mensagem/email, aplicadas à maneira de respostas e revisões de commit e mensagens gerais na lista de emails: <https://subspace.kernel.org/etiquette.html>.

Além de um código de conduta geral para desenvolvedores: <https://docs.kernel.org/process/code-of-conduct.html>.

Então, alguns códigos que não se adequam totalmente às regras do Linux e precisam de algum reparo, estão na pasta `drivers/staging` do kernel.
Nessa pasta há excelentes 'primeiros patches' que podemos buscar para contribuir.

Nós já separamos alguns aqui para vocês: <https://etherpad.wikimedia.org/p/lkcamp-1> (até lkcamp-4 tem patches para fazer :3).

Vocês podem por o nome dentro do `[<developer>]` para indicar que você está trabalhando nesse commit, para que duas pessoas não contribuam no mesmo.

Você pode acompanhar pelo [tutorial](https://docs.lkcamp.dev/intro_tutorials/first_contribution/#2-find-a-good-first-contribution) para compreender melhor o que fizemos nessa etapa.

### 3. Fazer a contribuicao

Contribuindo com algo que temos certeza que não adicionará possíveis pontos de falha no código,
podemos garantir que nosso commit está seguindo o coding style do Linux (rodando `checkpatch.pl`).
Seu commit não deve adicionar nenhum 'check' ou 'warnings' ao código.

Para enviar para as pessoas corretas, temos também uma ferramenta desenvolvida para facilitar essa busca:
`get_maintainer.pl` identifica os responsáveis (pessoas e listas) desse driver específico.

Não se esqueça de enviar na lista de emails do lkcamp também, assim, podemos ver suas contribuições e fazer review delas. :)

Todos os comandos estão indicados na [etapa 3 do tutorial](https://docs.lkcamp.dev/intro_tutorials/first_contribution/#3-making-a-contribution).

### 4. E depois de enviada a contribuição?

Agora você deve esperar o review da comunidade:
- caso seu código esteja dentro do esperado e seja valioso para a comunidade, ele seja adicionado na árvore `-next` do subsistema e na próxima merge window estará no código oficial do kernel. \o/
- caso haja uma sugestão de alteração, você deverá aplicá-la ao seu código, fazer uma segunda versão e reenviá-la nas listas de email, essa etapa está descrita na [etapa 4 do tutorial](https://docs.lkcamp.dev/intro_tutorials/first_contribution/#4-got-feedback-sending-a-second-version-of-a-patch).

Lembre-se que apesar de apenas o mantenedor poder aceitar seu commit na árvore do susbistema, toda a comunidade pode revisar o código.

E também, lembre-se de ser respeitoso nas comunidades.
Todes desenvolvedores querem apenas melhorar o código: as críticas são e devem ser relativas apenas ao código e devemos as entender como tal.

E bem-vindes ao desenvolvimento desse que é o maior projeto do mundo. :)

### 5. Testar sua contribuição

Idealmente você deve garantir que seu commit não quebrará nenhuma funcionalidade do kernel.

Ainda que seja impossível garantir que isso aconteça, pois há uma quantidade muito grande e variável de hardware que fazem uso do kernel,
podemos mitigar essas ocorrências testando o código em nossa máquina. Veja a sessão extra do tutorial para ver como podemos fazer isso: <https://docs.lkcamp.dev/intro_tutorials/first_contribution/#5-extra-sending-more-contributions>

Saber como testar seu código já te adiantará para os próximos projetos um pouco mais complexos.
