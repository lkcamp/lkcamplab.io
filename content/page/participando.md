---
title: "Participando"
date: 2022-02-19T19:20:00-03:00
draft: false
---

Se você tem interesse em projetos **FOSS**, como o **Kernel Linux**, e quer explorar o
código deles, aprender, ensinar, contribuir de volta para as comunidades, ou
conversar sobre, e ter pessoas com os mesmos interesses para te acompanhar e
ajudar nessa jornada, você está no lugar certo! :^)

Se já não tiver visto, veja **[os objetivos](/objetivos)** do grupo.

O principal canal de comunicação do grupo é o **[grupo do
Telegram][telegram]**! E é um bom lugar para tirar dúvidas fora do horário do
encontro semanal.

## Oficinas do início do semestre

Nesse começo de semestre teremos oficinas de contribuição do kernel. Essas
oficinas tem por objetivo iniciar novos desenvolvedores do kernel Linux para
posteriormente, desenvolvermos projetos mais complexos.

## Encontro semanal

Realizamos um encontro semanal híbrido na **Unicamp** e no
[link](https://meet.jit.si/lkcamp-unicamp): 

* **Quartas-feiras**: 19:00h às 23:00h ([horário de Brasília][brasilia-time])
na sala 352 do [IC-3.5][ic3-location].

Com exceção das oficinas inicias,
não uniformizamos tópicos para as reuniões supracitadas, geralmente, os tópicos
discutidos nessas reuniões obedecem as demandas que surgem naturalmente dentro
da entidade. As pautas também são discutidas eventualmente no grupo do telegram
no tópico: ['Organização - [semestre], [ano]'](https://t.me/lkcamp/21064).

Em geral, além de questões organizacionais, nas reuniões os participantes podem
mostrar os projetos nos quais estão trabalhando e compartilhar outras
experiências/sugestões. É um momento de compartilhamento e aprendizado. Todas
as pessoas (**sejam da Unicamp ou não**) estão convidadas a participar!

Os encontros a princípio não terão uma agenda. Qualquer pessoa é bem-vinda para
participar desenvolvendo seu projeto.

Todas as pessoas são encorajadas a interagir: conte sobre seu projeto para as
demais, pergunte sobre o projeto das outras. Se não tiver um projeto em mente e
quiser algum direcionamento por onde começar, peça ajuda! (e veja as sugestões
abaixo)

## Sugestões para começar

Se você ainda não tem um objetivo em mente, aqui vão algumas sugestões.

Fizemos uma série de vídeos/podcasts chamada [LkConf](/lkconf), em que
conversamos com contribuidores de grandes projetos colaborativos de software
livre (GNOME, KDE, Debian, Arch Linux, etc). Ver/ouvir essa série pode ser uma
boa forma de se inspirar na comunidade que move esses projetos e ter ideias de
onde você quer contribuir!

Caso você já tenha se interessado pelo Kernel Linux especificamente, ou então se
só ouviu falar sobre e gostaria de aprender mais, [temos uma playlist de vídeos](https://peertube.lhc.net.br/w/p/rYLMDq4ebGXLR7FJpU27ru)
inteirinha sobre ele! Nesses vídeos explicamos os fundamentos do funcionamento
do Kernel Linux de forma abrangente, com a intenção de preparar qualquer pessoa
para contribuir para esse formidável projeto. Também temos [exercícios práticos](https://docs.lkcamp.dev/unicamp_group/boot/)
para aprender colocando a "mão na massa". 

Se seu interesse principal for começar a contribuir com código, não existe forma
melhor do que baixar o código do projeto e rodar, procurando por problemas. No
caso específico do Kernel Linux, sugestões de contribuição são compilar algum
dos drivers em [staging](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/drivers/staging)
e rodar o script em [scripts/checkpatch.pl](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/scripts/checkpatch.pl)
nos arquivos e consertar algum dos warnings/erros presentes!

Ou talvez você queira é se debruçar sobre um tema a fundo e entendê-lo sem
necessariamente enviar uma contribuição. No caso do Kernel Linux, explore [o
código](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/),
[a documentação](https://www.kernel.org/doc/html/latest/) ou artigos no
[LWN](https://lwn.net/) ou pela internet e veja se encontra algum subsistema ou
funcionalidade específica do Kernel que desperte seu interesse e vá fundo!

E, claro, converse com outras pessoas durante o encontro semanal. Às vezes de
uma simples conversa pode surgir uma ideia que te interessa!

O que quer que você aprenda durante suas explorações, adoraríamos que você
explicasse para as demais pessoas do grupo durante uma apresentação ou apenas
informalmente, para que possamos expandir o conhecimento de todos!

## Links

Todos os links do **LKCAMP** [aqui](https://lkcamp.gitlab.io/lynx/home/)!

- Listas de email: [patches][mail-patches], [discussão][mail-discussion]
- Canais: [Youtube][youtube], [PeerTube][peertube]

[telegram]: https://t.me/lkcamp
[mail-patches]: https://lists.sr.ht/~lkcamp/patches
[mail-discussion]: https://lists.sr.ht/~lkcamp/discussion
[youtube]: https://www.youtube.com/channel/UC2skRId4WWg9F0vc6GAKRIA
[peertube]: https://peertube.lhc.net.br/c/lkcamp_main/videos
[ic2-location]: https://www.openstreetmap.org/#map=19/-22.81478/-47.06460
[ic3-location]: https://www.openstreetmap.org/#map=19/-22.81347/-47.06397
[brasilia-time]: https://www.timeanddate.com/worldclock/brazil/brasilia

