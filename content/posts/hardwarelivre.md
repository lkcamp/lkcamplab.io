---
title: "Hardware Livre"
date: 2023-06-01T11:18:36-03:00
draft: false
categories: ["Informação"]
tags: ["Liberdade", "Open Source Hardware", "Hardware", "Open source", "Livre"]
authors: ["Julio Nunes Avelar"]
---

# Hardware Livre

![Hardware Livre](/imgs/posts/hardwarelivre/logoopensourcehardware.png "image_tooltip")

>"Libere o Poder da Colaboração: Abraçando a Revolução do Hardware Livre!"

# Hardware Livre? Isso existe?

Provavelmente você deve estar se perguntando: Hardware Livre é algo que
realmente existe? Bom, antes de respondermos essa pergunta, vamos responder
primeiramente outra pergunta: o que é Hardware Livre?

# Antes de tudo o que é Hardware Livre?

Provavelmente você já deve estar imaginando do que se trata, mas de forma geral,
Hardware Livre se trata de projetos de hardware em que os diagramas, esquemas,
informações técnicas entre outras coisas, são disponibilizados de forma aberta e
livre para que outras pessoas possam estudar, modificar, distribuir e fabricar.
Assim como o software livre, o conceito de Hardware Livre promove a
transparência, a colaboração e o compartilhamento de conhecimento.

# Respondendo a pergunta inicial

Com base nas informações fornecidas anteriormente, podemos agora responder de
forma mais clara à pergunta inicial. Se considerado um hardware livre um
dispositivo onde além de seus esquemáticos todos os componentes também têm
diagramas e arquitetura disponibilizados de forma livre. Pode-se dizer que não
existe nenhum hardware totalmente livre. No entanto, se usarmos como parâmetro
apenas a existência dos diagramas e esquemáticos e outras coisas do gênero de
forma livre, temos diversos dispositivos com hardware livre.

Outro ponto importante a se analisar é o ponto de vista econômico e técnico. Por
mais que diversos projetos sejam extremamente livres, chegando a incluir até
mesmo a arquitetura de alguns chips de forma livre, existe o problema em que, na
maioria das vezes, um mero usuário comum, mesmo que portador de muito
conhecimento sobre hardware, não consegue fabricar seu próprio dispositivo com
base em algum projeto de hardware livre. Isso ocorre devido à necessidade de se
ter ferramentas e materiais extremamente caros.

# Como surgiu o Hardware Livre?

O conceito de Hardware Livre tem suas raízes no movimento de software livre, que
ganhou destaque nas décadas de 1980 e 1990 com a promoção de licenças e
filosofias que defendem a liberdade de uso, modificação e distribuição do
software. À medida que o software livre ganhava popularidade e aceitação, a
ideia de aplicar os mesmos princípios de liberdade ao hardware começou a surgir.
O termo "Hardware Livre" foi usado pela primeira vez por Richard Stallman,
fundador do movimento de software livre e da Free Software Foundation (FSF).
Stallman destacou a importância de extender os princípios do software livre ao
hardware, enfatizando a necessidade de liberdade e abertura não apenas no
código, mas também nos componentes físicos dos dispositivos eletrônicos.

Nos anos 2000, iniciativas e projetos relacionados ao Hardware Livre começaram a
surgir. Um marco significativo foi o lançamento da primeira versão da licença
CERN Open Hardware License (CERN OHL) em 2007, desenvolvida pelo CERN
(Organização Europeia para a Pesquisa Nuclear) para promover a colaboração e
compartilhamento de projetos de hardware.

Outro desenvolvimento importante foi o surgimento da arquitetura RISC-V na
Universidade da Califórnia em Berkeley em 2010. A RISC-V é uma arquitetura de
conjunto de instruções reduzidas (RISC) de código aberto, que permite o design e
a implementação de processadores personalizados e abertos.

A partir desses e de outros esforços, a comunidade do Hardware Livre foi
crescendo ao longo do tempo. Atualmente, existem várias comunidades,
organizações e projetos que promovem o Hardware Livre, como a Open Source
Hardware Association (OSHWA) e a Fundação RISC-V, que trabalham para estabelecer
padrões, licenças e práticas para apoiar o desenvolvimento de hardware aberto e
colaborativo.

# Licenças no Hardware Livre?

No campo do hardware livre, assim como no software, existem várias licenças que
podem ser usadas para garantir a abertura e a liberdade dos projetos. Essas
licenças definem os termos e condições de uso, modificação, distribuição e
fabricação do hardware. Aqui estão algumas das licenças comumente usadas no
contexto do hardware livre:

1. CERN Open Hardware License (CERN OHL): Desenvolvida pelo CERN, é uma licença
   amplamente utilizada no campo do Hardware Livre. Ela permite a distribuição,
   modificação e fabricação do hardware, exigindo que as versões modificadas
   sejam compartilhadas sob a mesma licença.
2. TAPR Open Hardware License: Criada pela TAPR (Tucson Amateur Packet Radio), é
   uma licença popular entre os projetos de rádio amador e comunicações. Ela
   permite a modificação, fabricação e distribuição do hardware, com a obrigação
   de manter as atribuições de autoria.
3. Solderpad Hardware License: Uma licença de Hardware Livre que promove a
   colaboração e o compartilhamento de projetos. Ela permite a modificação,
   fabricação e distribuição do hardware, exigindo que as versões modificadas
   sejam compartilhadas sob a mesma licença.
4. Creative Commons (CC): Embora as licenças Creative Commons sejam mais
   conhecidas no contexto do conteúdo digital, algumas variantes podem ser
   adaptadas para o Hardware Livre. Por exemplo, a licença Creative Commons
   Attribution-ShareAlike (CC BY-SA) pode ser aplicada ao hardware, permitindo o
   uso, modificação e distribuição, desde que a atribuição seja fornecida e as
   versões modificadas sejam compartilhadas sob a mesma licença.

Essas são apenas algumas das licenças comumente usadas no Hardware Livre, e cada
uma possui suas próprias características e requisitos específicos. É importante
revisar cuidadosamente os termos de cada licença e escolher a que melhor se
alinha aos objetivos do projeto de Hardware Livre em questão. Um detalhe a se
notar é que no início do Hardware Livre era utilizado as mesmas Licenças de
software livre, mas por questões legais surgiu a necessidade de ter licenças
focadas para a necessidade específica do desenvolvimento de Hardware Livre, com
essas licenças garantindo um maior aparato legal aos desenvolvedores.

# Vantagens do Hardware Livre

O Hardware Livre oferece uma série de benefícios para os usuários,
desenvolvedores e a comunidade em geral. Alguns desses benefícios incluem:

* Transparência: O Hardware Livre permite que os usuários tenham acesso completo
  aos detalhes técnicos do dispositivo, incluindo esquemas, diagramas e
  documentação. Isso promove a transparência e a compreensão do funcionamento
  interno do dispositivo.
* Liberdade de modificação: Com o Hardware Livre, os usuários têm a liberdade de
  modificar o dispositivo de acordo com suas necessidades. Eles podem adicionar
  ou remover recursos, personalizar o design e adaptar o hardware para
  diferentes aplicações.
* Colaboração: A natureza aberta do Hardware Livre incentiva a colaboração e o
  compartilhamento de conhecimento entre os desenvolvedores. Isso leva a uma
  comunidade ativa e engajada, que trabalha em conjunto para melhorar os
  projetos existentes e criar novas soluções.
* Aprendizado e educação: O Hardware Livre é uma excelente ferramenta para
  aprendizado e educação em engenharia, eletrônica e ciência da computação. Os
  estudantes podem estudar os projetos existentes, entender como os dispositivos
  funcionam e até mesmo criar suas próprias versões.
* Redução do desperdício eletrônico: Com o acesso aos esquemas e diagramas, os
  usuários podem reparar e atualizar os dispositivos existentes em vez de
  descartá-los. Isso contribui para a redução do desperdício eletrônico e
  promove uma mentalidade de sustentabilidade.

# Alguns projetos

Atualmente existem diversos projetos de Hardware Livre, com alguns tendo uma
barreira para contribuição menores do que outros, alguns mais bem difundidos e
outros em estados mais complexos. Para facilitar a sua vida como leitor
listarei alguns projetos interessantes para se testar e/ou contribuir.

* Caninos Loucos: [https://caninosloucos.org/pt/](https://caninosloucos.org/pt/)
  , um projeto brasileiro que visa desenvolver placas totalmente Hardware Livre
  e de baixo custo, com o projeto tendo 2 placas SBCs (Single Board Computer) e
  1 placa microcontroladora desenvolvida. Possui possibilidade de contribuição
  diretamente com o hardware ou para o software através de sdks, RTOS e
  bibliotecas.

Placa Labrador Caninos Loucos - Fonte Caninos Loucos

![Caninos Labrador](/imgs/posts/hardwarelivre/caninoslabrador.png "image_tooltip")

* Uzebox: [https://uzebox.org/](https://uzebox.org/) , um video game retrô de 8
  bits, totalmente open source. É muito legal para aprender eletrônica enquanto
  se diverte.

Video game retrô Uzeboz - Fonte: Uzebox

![Video game retrô Uzeboz](/imgs/posts/hardwarelivre/uzeboz.png "image_tooltip")

* Projeto RepRap:
  [https://reprap.org/wiki/RepRap](https://reprap.org/wiki/RepRap) , uma
  impressora 3d auto replicante. O Projeto RepRap é uma iniciativa de código
  aberto que visa desenvolver uma impressora 3D capaz de imprimir seus próprios
  componentes. O nome "RepRap" é uma combinação de "replicating" (replicando) e
  "rap" (rapidez), indicando a capacidade da impressora de se reproduzir e
  evoluir rapidamente.

Impressora 3d - Fonte: RepRap

![Impressora 3d](/imgs/posts/hardwarelivre/impressora3d.png "image_tooltip")

* Arduino: [https://www.arduino.cc/](https://www.arduino.cc/) , Uma placa de
  prototipagem open-source de fácil utilização, com milhares de bibliotecas open
  sources e altamente difundida entre *makers*.

Placa Arduino UNO R3 - Fonte: MakerHero

![Arduino UNO R3](/imgs/posts/hardwarelivre/arduino.png "image_tooltip")

* Franzininho: [https://franzininho.com.br/](https://franzininho.com.br/) , O
  projeto Franzininho é uma iniciativa brasileira de código aberto que visa
  criar uma placa microcontroladora de baixo custo e fácil de usar,
  especialmente projetada para iniciantes na área de eletrônica e programação. O
  nome "Franzininho" é uma homenagem ao inventor brasileiro Francisco
  "Franzininho" Sampaio, conhecido por seu trabalho com Arduino e eletrônica de
  baixo custo.

Franzininho Wifi - Fonte: Embarcados

![Franzininho Wifi](/imgs/posts/hardwarelivre/franzininhowifi.jpg "image_tooltip")

# Software

O hardware e o software são dois componentes essenciais e interdependentes. O
Hardware Livre depende do software para alcançar seu pleno potencial. Afinal, de
que adianta ter um dispositivo se não houver os drivers e o software adequado
para utilizá-lo corretamente? Portanto, é fundamental reconhecer a importância
do software no contexto do Hardware Livre. No desenvolvimento de Hardware Livre,
há diversas áreas em que o software desempenha um papel fundamental. Alguns
exemplos incluem:

* Drivers e firmware: Os drivers são programas que permitem a comunicação entre
  o hardware e o sistema operacional, garantindo que os dispositivos funcionem
  corretamente. No caso do hardware livre, é necessário desenvolver drivers que
  sejam compatíveis e suportem as funcionalidades específicas dos dispositivos
  de hardware livre. Além disso, o firmware é um tipo especial de software que
  está embutido nos dispositivos de hardware e controla seu funcionamento
  interno.
* Sistemas operacionais: O software livre também desempenha um papel fundamental
  no desenvolvimento de sistemas operacionais para dispositivos de hardware
  livre. Os sistemas operacionais fornecem uma interface entre o usuário e o
  hardware, permitindo a execução de programas e o gerenciamento dos recursos do
  dispositivo. O desenvolvimento de sistemas operacionais livres para hardware
  livre garante que os usuários tenham liberdade para modificar, adaptar e
  distribuir o sistema operacional de acordo com suas necessidades.
* Aplicações e bibliotecas: Além dos drivers e sistemas operacionais, o
  desenvolvimento de software livre no contexto do Hardware Livre também abrange
  o desenvolvimento de aplicações e bibliotecas. As aplicações são programas que
  os usuários finais utilizam para realizar tarefas específicas, enquanto as
  bibliotecas são conjuntos de funções e recursos que podem ser utilizados por
  outros programas. Desenvolver aplicações e bibliotecas livres para hardware
  livre permite que os usuários personalizem e expandam as funcionalidades de
  seus dispositivos de acordo com suas necessidades e interesses.
* Ferramentas de desenvolvimento: O desenvolvimento de Hardware Livre requer
  ferramentas de software específicas que permitam projetar, simular e testar os
  dispositivos. Isso inclui ferramentas de design de circuitos, simulação,
  programação e depuração. O desenvolvimento de ferramentas de software livre
  para Hardware Livre garante que os desenvolvedores tenham acesso a recursos
  adequados para criar e aprimorar seus projetos.
* RTOS (Real-Time Operating Systems): Um RTOS é um sistema operacional projetado
  para atender a requisitos de tempo real, garantindo respostas rápidas e
  previsíveis a eventos e interrupções. No desenvolvimento de Hardware Livre, o
  uso de um RTOS livre é benéfico para dispositivos que exigem controle em tempo
  real, como sistemas embarcados, robôs e automação industrial. O
  desenvolvimento de RTOS livres permite que os usuários personalizem e otimizem
  o sistema operacional para suas aplicações específicas, garantindo um
  desempenho confiável e determinístico.

A abrangência de áreas de software permite que desenvolvedores, mesmo aqueles
que não têm interesse ou habilidade em lidar diretamente com o hardware, possam
contribuir para projetos de hardware livre. Isso significa que pessoas com
conhecimentos em programação e desenvolvimento de software têm a oportunidade de
colaborar com a comunidade do hardware livre, oferecendo suas habilidades na
criação de drivers, aplicativos, bibliotecas e outras soluções de software que
ampliam a funcionalidade e a interoperabilidade dos dispositivos de hardware
livre. Essa colaboração é essencial para impulsionar a adoção e o avanço do
hardware livre, tornando-o acessível a um público mais amplo e estimulando a
inovação colaborativa.

# Projetos de Software interessantes para se contribuir

Atualmente, existem várias iniciativas de software nas categorias mencionadas
que desempenham um papel fundamental no desenvolvimento de Hardware Livre. Aqui
estão alguns exemplos desses softwares:

* Zephyr RTOS: O Zephyr é um sistema operacional de tempo real de código aberto,
  projetado para dispositivos embarcados e de IoT. Ele oferece suporte a uma
  ampla variedade de arquiteturas e possui uma comunidade ativa de
  desenvolvedores. O Zephyr RTOS é amplamente utilizado em projetos de hardware
  livre devido à sua flexibilidade, eficiência e recursos avançados.

Logo Zephyr RTOS - Fonte: Wikipedia

![Logo Zephyr RTOS](/imgs/posts/hardwarelivre/zephyrrtos.png "image_tooltip")

* LibCamera: A LibCamera é uma biblioteca de código aberto que fornece uma API
  unificada para acesso a câmeras em sistemas Linux. Ela simplifica o
  desenvolvimento de aplicativos que utilizam câmeras, fornecendo uma interface
  consistente e abstração de hardware. A LibCamera é frequentemente usada em
  projetos de Hardware Livre que envolvem câmeras e visão computacional.

Logo LibCamera - Fonte: Github

![alLibCamerat_text](/imgs/posts/hardwarelivre/logolibcamera.png "image_tooltip")

* Chipyard: O Chipyard é um framework de código aberto para a criação e design
  de sistemas de computação baseados em chips RISC-V. Ele fornece uma
  infraestrutura completa para o desenvolvimento de chips personalizados,
  incluindo ferramentas de design, fluxos de trabalho e modelos de referência. O
  Chipyard é utilizado em projetos de Hardware Livre que visam criar
  processadores RISC-V personalizados e explorar a arquitetura aberta.

Logo ChipYard - Fonte: ChipYard

![Logo ChipYard](/imgs/posts/hardwarelivre/logochipyard.png "image_tooltip")

Esses são apenas alguns exemplos de softwares que desempenham um papel
importante no desenvolvimento de Hardware Livre. No entanto, é importante
ressaltar que existem muitas outras iniciativas e ferramentas disponíveis.

# Como contribuir

A forma de contribuição varia significativamente de um projeto para outro, com
muitos deles sendo hospedados em plataformas populares como GitHub e GitLab,
enquanto outros optam por meios de distribuição e contribuição próprios.

Nos projetos hospedados no GitHub e GitLab, os mantenedores frequentemente
disponibilizam "issues" que listam tarefas a serem corrigidas ou implementadas.
Essas issues permitem que os colaboradores identifiquem áreas em que podem
contribuir. Ao utilizar essas plataformas, você pode examinar as issues abertas
em um projeto e identificar tarefas específicas que precisam ser implementadas.

Uma abordagem comum é fazer um fork (uma cópia independente) do projeto
original, realizar as modificações necessárias no seu fork e, em seguida, enviar
um "merge request" para os mantenedores do projeto original. Isso permite que
eles revisem suas alterações e as incorporem ao projeto principal, se
considerarem adequadas.

Essa abordagem de "fork" e "merge request" facilita a colaboração em projetos de
open sources, permitindo que desenvolvedores de diferentes partes do mundo
contribuam com suas habilidades e ideias para melhorar o projeto. Além disso,
muitas plataformas também oferecem recursos de discussão e comunicação, como
comentários em issues e pull requests, que ajudam na troca de informações entre
os colaboradores.

É importante destacar que cada projeto pode ter suas próprias diretrizes e
processos de contribuição. Portanto, é sempre recomendado consultar a
documentação e as orientações específicas de cada projeto antes de iniciar sua
contribuição. Isso garantirá uma melhor compreensão das práticas recomendadas e
ajudará a otimizar o processo de colaboração.

# Projetos que estou contribuindo atualmente

* Caninos Loucos: Estou contribuído com o desenvolvimento do SDK da placa
  labrador, para permitir a utilização de recursos da mesma utilizando a
  linguagem de programação Python.
* Bibliotecas para LoRa: Atualmente estou portando bibliotecas para possibilitar
  o uso a tecnologia LoRa com diversos microcontroladores.
* Projetos Próprios: Atualmente estou desenvolvendo diversos projetos de
  hardware e os disponibilizando de forma open source. Tais projetos incluem
  estações meteorológicas, CubeSATs e outras coisas.

# Referências para aprender

* Devopedia:
  [https://devopedia.org/open-source-hardware](https://devopedia.org/open-source-hardware)
* Projeto GNU:
  [https://www.gnu.org/philosophy/free-hardware-designs.pt-br.html](https://www.gnu.org/philosophy/free-hardware-designs.pt-br.html)
* CCSL USP: [https://ccsl.ime.usp.br/](https://ccsl.ime.usp.br/)
* LKCAMP: [https://lkcamp.dev](https://lkcamp.dev)
* BZOIDE: [https://bzoide.dev](https://bzoide.dev)
* Franzininho: [https://franzininho.com.br/](https://franzininho.com.br/)
* Embarcações: [https://gitlab.com/embarcacoes](https://gitlab.com/embarcacoes)

# Algumas comunidades

Existem diversas comunidades que se dedicam ao estudo e desenvolvimento de
Hardware Livre. Algumas delas são:

* Grupo de estudos em Sistemas Embarcados e Hardware da Unicamp (Embarcações):
  Essa comunidade é voltada para estudantes e entusiastas interessados em
  sistemas embarcados e hardware. É possível acessar o telegram da comunidade
  pelo seguinte link:
  [https://t.me/+PdSj1sTUFSIyOGVh](https://t.me/+PdSj1sTUFSIyOGVh)
* Grupo de afinidade de Hardware Livre do LKCAMP: O LKCAMP é uma comunidade
  focada em software livre, e possui um grupo de afinidade específico para
  Hardware Livre. Você pode participar desse grupo no seguinte link:
  [https://t.me/lkcamp/15255](https://t.me/lkcamp/15255 )

# Fontes

Uzebox.  Wiki. Disponivel em: <https://uzebox.org/> Acesso em: 30/05/2023

FRANZININHO.  Sobre o FRANZININHO. Disponivel em:
<https://franzininho.com.br/#sobre> Acesso em: 30/05/2023

Arduino.  Arduino Hardware. Disponivel em: <https://www.arduino.cc/en/hardware>
Acesso em: 30/05/2023

RepRap.  Wiki. Disponivel em: <https://reprap.org/wiki/RepRap> Acesso em:
30/05/2023

Caninos Loucos.  Programa. Disponivel em:
<https://caninosloucos.org/pt/program-pt/> Acesso em: 30/05/2023

EMBARCADOS. Open source hardware: Conheça a definição e as boas práticas.
Disponível em: <https://embarcados.com.br/open-hardware-definicao/> Acesso em:
30/05/2023

WikiPédia. Hardware Livre. Disponivel em:
<https://pt.wikipedia.org/wiki/Hardware_livre> Acesso em: 30/05/2023

>"Assim como o software livre liberta o código, o Hardware Livre liberta a
>essência do mundo físico. É a liberdade em suas mãos, moldando o futuro por meio
>da tecnologia que você pode ver, tocar e transformar."
